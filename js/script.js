/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function () {
    'use strict';
    // Banner with animated caption
   /* $('.banner').bxSlider({
        controls: false,
        pager: false,
        mode: 'horizontal',
        auto: true,
        preloadImages: 'all',
        pause: 5000,
        speed: 1000,
        easing: 'ease-in-out',
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
        }
    });
*/
    // Testimonial
    $('.testimonial').bxSlider({
        auto: true,
        pager: false,
        nextSelector: '#slider-next',
        prevSelector: '#slider-prev',
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>'
    });
    
    $(window).scroll(function () {
        var windTop = $(window).scrollTop();
        
        // Navigation shrink
        if (windTop >= 200) {
            $('.header').addClass('shrink');
        } else {
            $('.header').removeClass('shrink');
        }
        
        // Go top link
        if (windTop >= 300) {
            $('.go_top').fadeIn();
        } else {
            $('.go_top').fadeOut();
        }
    });
    
    // Go top link
    $('.go_top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, {
            duration: 1500,
            easing: 'easeInOutCirc' //easeInOutQuint easeInOutExpo easeInOutCirc
        });
    });

    /*$('.biz_slider').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 2,
        slideMargin: 60,
        controls: false,
        auto: true
    });*/

    // Team Ridhi slider
    /*$('.team_slider').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 3,
        pager: false,
        auto: true,
        nextSelector: '#team_next',
        prevSelector: '#team_prev',
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>'
    });*/

    if ($(window).width() >= 992) {
        $('.biz_slider').bxSlider({
            slideWidth: 300,
            minSlides: 1,
            maxSlides: 2,
            slideMargin: 60,
            controls: false,
            auto: true
        });

        $('.team_slider').bxSlider({
            slideWidth: 300,
            minSlides: 1,
            maxSlides: 3,
            pager: false,
            auto: true,
            nextSelector: '#team_next',
            prevSelector: '#team_prev',
            nextText: '<i class="fa fa-chevron-right"></i>',
            prevText: '<i class="fa fa-chevron-left"></i>'
        });
    } else if (($(window).width() <= 991) && ($(window).width() >= 768)) {
        $('.biz_slider').bxSlider({
            slideWidth: 350,
            minSlides: 1,
            maxSlides: 2,
            slideMargin: 60,
            controls: false,
            auto: true
        });

        $('.team_slider').bxSlider({
            slideWidth: 300,
            minSlides: 1,
            maxSlides: 2,
            pager: false,
            auto: true,
            nextSelector: '#team_next',
            prevSelector: '#team_prev',
            nextText: '<i class="fa fa-chevron-right"></i>',
            prevText: '<i class="fa fa-chevron-left"></i>'
        });
    } else {
        $('.biz_slider').bxSlider({
            slideWidth: 290,
            minSlides: 1,
            maxSlides: 3,
            controls: false,
            auto: true,
            mode: 'fade'
        });

        $('.team_slider').bxSlider({
            slideWidth: 290,
            minSlides: 1,
            maxSlides: 2,
            pager: false,
            auto: true,
            mode: 'fade',
            nextSelector: '#team_next',
            prevSelector: '#team_prev',
            nextText: '<i class="fa fa-chevron-right"></i>',
            prevText: '<i class="fa fa-chevron-left"></i>'
        });
    }
    
    // Navigation
    $('.nav_trigger').click(function () {
        $('ul.navigation').slideToggle();
    });
    $(window).resize(function () {
        if ($(window).width() >= 768) {
            $('ul.navigation').show();
        } else {
            $('ul.navigation').hide();
        }
    });
    
        // mouseover effect on 
    var preloadList = [];
    $('.what_block a img').each(function () {
        preloadList.push($(this).data('update'));
    });
    //console.log(preloadList);
    $(preloadList).preloadImg();
});
(function ($){
    $.fn.preloadImg = function(){
        this.each(function(){
            $('<img/>')[0].src = this;
        });    
    };
}(jQuery));